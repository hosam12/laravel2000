<!doctype html>
<html lang="en">

<head>
<title>Techno Band | @yield('title')</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
<meta name="keywords" content="admin template, Oculux admin template, dashboard template, flat admin template, responsive admin template, web app, Light Dark version">
<meta name="author" content="GetBootstrap, design by: puffintheme.com">
<link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('cms/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('cms/assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('cms/assets/vendor/animate-css/vivify.min.css')}}">
{{-- file --}}
<link rel="stylesheet" href="{{asset('cms/assets/vendor/dropify/css/dropify.min.css')}}">
<link rel="stylesheet" href="{{asset('cms/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
{{-- endfile --}}
{{-- compose --}}
<link rel="stylesheet" href="{{asset('cms/assets/vendor/summernote/dist/summernote.css')}}"/>

<link rel="stylesheet" href="{{asset('cms/assets/vendor/c3/c3.min.css')}}"/>
<link rel="stylesheet" href="{{asset('cms/assets/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">

<!-- MAIN CSS -->
<link rel="stylesheet" href="{{asset('cms/assets/vendor/light-gallery/css/lightgallery.css')}}">

<link rel="stylesheet" href="{{asset('cms/html/assets/css/site.min.css')}}">

</head>
<body class="theme-orange font-montserrat">

<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>


<!-- Theme Setting -->

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<div id="wrapper">

    <nav class="navbar top-navbar">
        <div class="container-fluid">

            <div class="navbar-left">
                <div class="navbar-btn">
                <a href="#"><img src="{{asset('cms/assets/images/icon.svg')}}" alt="Oculux Logo" class="img-fluid logo"></a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
                <ul class="nav navbar-nav">

                        <a   onclick="noti()" href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <li class="dropdown">
                            @if (Auth::user()->unreadNotifications()->count()>0)
                            <span id="noti" class="notification-dot bg-azura">{{Auth::user()->unreadNotifications()->count()}}</span>
                            @endif
                            <i class="icon-bell"></i>
                        </a>
                        <ul class="dropdown-menu feeds_widget vivify fadeIn">
                            <li class="header blue">الإشعارات</li>
                            <li>
                                @foreach (Auth::user()->Notifications as $item)
                                @if ($item->data['type']=='order')
                                <a href="{{route('order.show',[$item->data['id']])}}">
                                    @if($item->read_at)

                                    <div class="feeds-left bg-green"><i class="fa fa-check"></i></div>
                                            <div class="feeds-body">
                                        <h4 class="title text-red">طلبية <small class="float-right text-muted">{{$item->created_at->diffForHumans()}}</small></h4>

                                    </div>
                                    @else
                                                           <div class="feeds-body">
                                        <h4 class="title text-danger">{{$item->data['title']}} <small class="float-right text-muted">{{$item->created_at->diffForHumans()}}</small></h4>

                                    </div>
                            <div class="feeds-left bg-red"><i class="fa fa-check"></i></div>
                            @endif

                                @elseif($item->data['type']=='user')

                                </a>
                                      <a href="{{route('user.show',[$item->data['id']])}}">
                                         @if($item->read_at)
                                    <div class="feeds-left bg-green"><i class="fa fa-user"></i></div>


                                    @else
                                <div class="feeds-left bg-red"><i class="fa fa-user"></i></div>
                                    @endif

                                    <div class="feeds-body">
                                        <h4 class="title text-danger">{{$item->data['title']}} <small class="float-right text-muted">{{$item->created_at->diffForHumans()}}</small></h4>
                                        <small>{{$item->data['title']}}</small>
                                    </div>
                                </a>
                                 @elseif($item->data['type']=='contact')
                                      <a href="{{route('contact.show',[$item->data['id']])}}">
                                         @if($item->read_at)
                                    <div class="feeds-left bg-green"><i class="icon-envelope"></i></div>


                                    @else
                                <div class="feeds-left bg-red"><i class="icon-envelope"></i></div>
                                    @endif

                                    <div class="feeds-body">
                                        <h4 class="title text-danger">{{$item->data['title']}} <small class="float-right text-muted">{{$item->created_at->diffForHumans()}}</small></h4>
                                        <small>{{$item->data['title']}}</small>
                                    </div>
                                </a>

                                @endif
                                @endforeach

                            </li>


                        </ul>
                    </li>

                <li><a href="{{route('admin.dashbord')}}" class="megamenu_toggle icon-menu" style="font-size: 20px" title="الرئيسية">الرئيسية</a></li>
                <li><a href="{{route('ser.index')}}" class="megamenu_toggle icon-menu" style="font-size: 20px" title="الميزات ">الميزات</a></li>
                <li><a href="{{route('comp.index')}}" class="megamenu_toggle icon-menu" style="font-size: 20px" title="الماركات التجارية ">الماركات التجارية</a></li>

                </ul>
            </div>

            <div class="navbar-right">
                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li><a href="javascript:void(0);" class="search_toggle icon-menu" title="Search Result"><i class="icon-magnifier"></i></a></li>
                        <li><a href="{{route('admin.logout')}}" class="icon-menu"><i class="icon-power"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>
    </nav>

    <div class="search_div">
        <div class="card">
            <div class="body">
            <form id="navbar-search" method="POST" action="{{route('search.admin')}}" class="navbar-form search-form">
                @csrf
                    <div class="input-group mb-0">
                    <input type="text" name="search" value="{{request()->input('search')}}" class="form-control" placeholder="البحث...">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text"><i class="icon-magnifier"></i></button>
                            <a href="javascript:void(0);" class="search_toggle btn btn-danger"><i class="icon-close"></i></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>




    <div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
            <a href="{{route('admin.dashbord')}}"><img src="{{asset('cms/assets/images/icon.svg')}}" alt="Oculux Logo" class="img-fluid logo"><span>Techno Band Admin</span></a><br>

            <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
        </div>
        <div class="sidebar-scroll">
            <div class="user-account">
                <div class="user_div">
                    <a href="{{route('admin.show',[Auth::user()->id])}}">
                  <img src="{{url('images/admins/'.Auth::user()->image)}}" class="user-photo" alt="User Profile Picture">

                    </a>
                </div>
                <div class="dropdown">
                    <span style="font-size: 20px">مرحبا بك</span>
                    <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>{{Auth::user()->name}}</strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                    <li><a href="{{route('admin.show',[Auth::user()->id])}}"><i class="icon-user"></i>صفحتي الشخصية</a></li>
                        <li><a href="{{route('admin.logout')}}"><i class="icon-power"></i>خروج</a></li>
                    </ul>
                </div>
            </div>
            <nav id="left-sidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="header" style="font-size: 20px"> الأساسية</li>
                     <li><a href="{{route('admin.dashbord')}}"><i class="icon-home"></i><span style="font-size: 20px">الرئيسية</span ></a></li>

                      <li >
                        <a href="#Contact" ><i class="icon-cursor"></i><span style="font-size: 20px">الفئات</span></a>
                        <ul  >
                            <li><a href="{{route('category.index')}}" style="font-size: 20px" >جميع الفئات</a></li>
                            <li><a href="{{route('category.create')}}" style="font-size: 20px">فئة جديدة</a></li>
                        </ul>
                    </li>

                     <li >
                        <a href="#Contact" ><i class="icon-list"></i><span style="font-size: 20px">المنتجات</span></a>
                        <ul  >
                            <li><a href="{{route('product.index')}}" style="font-size: 20px" >جميع المنتجات</a></li>
                            <li><a href="{{route('product.create')}}" style="font-size: 20px">منتج جديد</a></li>
                        </ul>
                    </li>
                      <li >
                        <a href="#Contact" ><i class="icon-users"></i><span style="font-size: 20px">المسوقين</span></a>
                        <ul  >
                            <li><a href="{{route('user.index')}}" style="font-size: 20px" >جميع المسوقين</a></li>
                            <li><a href="{{route('user.create')}}" style="font-size: 20px">مسوق جديد</a></li>
                        </ul>
                    </li>
                <li><a href="{{route('order.index')}}"><i class="icon-grid"></i><span style="font-size: 20px" >الطلبات</span ></a></li>
                 <li><a href="{{route('details.index')}}"><i class="icon-star"></i><span style="font-size: 20px" >تفصيل الطلبات</span ></a></li>
                <li ><a href="#" data-toggle="modal" data-target="#createcate" class="nav-link"><i class="icon-speedometer"></i><span style="font-size: 20px">فرز المهام</span></a></li>

                        <li >
                        <a href="#Contact" ><i class="icon-envelope"></i><span style="font-size: 20px">التواصل</span></a>
                        <ul  >
                            <li><a href="{{route('contact.create')}}" style="font-size: 20px" > إرسال رسالة</a></li>
                            <li><a href="{{route('contact.index')}}" style="font-size: 20px"> الرسائل المرسلة</a></li>
                            <li><a href="{{route('contact.resiver')}}" style="font-size: 20px">الرسائل المستلمة</a></li>

                        </ul>
                    </li>
                    <li><a href="{{route('about.create')}}"><i class="icon-cursor"></i><span style="font-size: 20px">حول الشركة</span></a></li>

                     <li><a href="{{route('general.index')}}"><i class="icon-equalizer"></i><span style="font-size: 20px"> الإعدادات العامة للموقع</span></a></li>
                    <li><a href="{{route('admin.show',[Auth::user()->id])}}"><i class="icon-badge"></i><span style="font-size: 20px"> الصفحة الشخصية</span></a></li>
                </ul>
            </nav>
        </div>
    </div>

  @yield('content')

</div>
@include('sweetalert::alert')


<!-- Javascript -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>

    function noti() {
        // console.log("wqfwsf");
                // document.getElementById("noti").hidden=true;

            axios.get('/cms/admin/noti')
                .then(function (response) {
                              $('#noti').hide();
                    console.log(10);


                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }
</script>

<script src="{{asset('cms/html/assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('cms/html/assets/bundles/vendorscripts.bundle.js')}}"></script>

<script src="{{asset('cms/html/assets/bundles/c3.bundle.js')}}"></script>
<script src="{{asset('cms/html/assets/bundles/jvectormap.bundle.js')}}"></script>
{{-- file --}}
<script src="{{asset('cms/assets/vendor/dropify/js/dropify.min.js')}}"></script>
<script src="{{asset('cms/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script><!-- bootstrap datepicker Plugin Js -->
<script src="{{asset('cms/html/assets/js/pages/forms/dropify.js')}}"></script>
{{-- end file --}}
<script src="{{asset('cms/html/assets/bundles/mainscripts.bundle.js')}}"></script>
<script src="{{asset('cms/html/assets/js/jobdashboard.js')}}"></script>
<script src="{{asset('cms/assets/vendor/summernote/dist/summernote.js')}}"></script>


@yield('script')
</body>

<!-- Modal -->
<div id="createcate" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">فرز المهام</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="addform" action="{{route('details.data')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1" style="font-size: 20px">من</label>
                      <div class="col-md-3 col-sm-6">
                            <div class="input-group">
                                <input name="from" type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Select Date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" style="font-size: 20px">إلى</label>
                       <div class="col-md-3 col-sm-6">
                            <div class="input-group">
                            <input name="to" type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" placeholder="Select Date">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="add" class=" btn btn-primary">ذهاب</button>
                        <button type="button" class="btn btn-danger " data-dismiss="modal">إغلاق</button>
                    </div>
                </form>
            </div>


        </div>

    </div>
</div>

</html>
