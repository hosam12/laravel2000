@extends('cms.admin.parent')
@section('title','طلبية')
@section('content')


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1>طلبية جديدة</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('admin.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('order.index')}}">الطلبيات</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">طلبية</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="body">
                            <article class="media">
                                <div class="mr-3">
                                <img class="rounded mr-3 w60 "  src="{{url('images/users/'.$order->user->image)}}" alt="{{$order->user->name}}">
                                </div>
                                <div class="media-body">
                                    <div class="content">
                                        <p>اسم المسوق</p>
                                        <h4>{{$order->user->name}}</h4>
                                        <p>اسم الزبون</p>
                                        <p>{{$order->name}}</p>
                                        <p>رقم الواتس للزبون </p>
                                        <p>{{$order->whats}}</p>
                                        <p> رقم الطلبية // {{$order->id}} </p>

                                    </div>

                                </div>
                            </article>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th> اسم المنتج</th>
                                                    <th>فئة المنتج</th>
                                                    <th>كود المنتج</th>
                                                    <th>سعر الجملة </th>
                                                    <th>سعر بيع القطعة الواحدة</th>
                                                    <th>العدد</th>
                                                    <th>ربح الشخص</th>
                                                    <th style="width: 80px;">المجموع</th>
                                                    <th style="width: 80px;">حالة التسليم</th>

                                                    <th>الإعدادات</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <span hidden>{{$i=0}}</span>
                                                @foreach ($order->details as $details)
                                                   <span hidden>{{$i++}}</span>
                                                      <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$details->product->name}}</td>
                                                            <td>{{$details->product->category->name}}</td>
                                                            <td>{{$details->product->code}}</td>
                                                            <td>{{$details->product->realprice}}</td>
                                                            <td>{{$details->currentPrice}}</td>
                                                            <td>{{$details->count}}</td>
                                                            <td>{{$details->profit}}</td>
                                                            <td>{{$details->total}}</td>
                                                               <td>
                                         @if($details->order->status=='ublode')
                                        <span style="font-size: 17px" class="badge badge-light">لم يتم الرفع</span>
                                        @elseif($details->order->status=='waiting')
                                    <span style="font-size: 17px" class="badge badge-warning">قيد الإعتماد</span>
                                     @elseif($details->status=='cancel'||$details->order->status=='cancel')
                                    <span style="font-size: 17px" class="badge badge-danger">مرفوضة</span>
                                      @elseif($details->status=='success')
                                    <span style="font-size: 17px" class="badge badge-success">تم التسليم</span>
                                    @else
                                    <span style="font-size: 17px" class="badge badge-light">____</span>



                                        @endif

                                    </td>
                                                            <td>
                                        <a href="{{route('details.edit',[$details->id])}}" type="button" style="font-size: 20px" class="btn btn-sm btn-default" title="تعديل"><i class="fa fa-edit"></i> تعديل</a>
                                        <a href="{{route('daw.order',[$details->image])}}" class="btn btn-success"> تنزيل صورة المنتج </a>

                                        {{-- <a onclick="confirmDelete(this, '{{$user->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a> --}}


                                                            </td>

                                                </tr>
                                                @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <form action="{{route('status.order',[$order->id])}}" method="POST">
                        @method("PUT")
                        @csrf
                        <div class="card-footer">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <h5>حالة التسليم</h5>
                            <div class="form-group">

                          <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="waiting"
                                        name="status" value="waiting"
                                        @if($order->status == 'waiting') checked @endif>
                                    <label style="font-size: 15px" for="waiting" class="custom-control-label">تم الرفع</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="success"
                                            name="status" value="success"
                                        @if($order->status == 'success') checked @endif>
                                    <label style="font-size: 15px" for="success" class="custom-control-label"> تم الرد</label>
                                </div>
                                  <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="cancel"
                                        name="status" value="cancel"
                                        @if($order->status == 'cancel') checked @endif>
                                    <label style="font-size: 15px" for="cancel" class="custom-control-label">مرفوضة</label>
                                </div>

                            </div>                                </div>
                                <div class="col-md-6 text-right">
                                <h3 class="m-b-0 m-t-10">المجموع الكلي للمنتجات //  {{$order->details->sum('total')}} </h3>
                                <h3 class="m-b-0 m-t-10">مجموع المنتجات الناجحة //  {{$order->details->where('status','success')->sum('total')}} </h3>
                                <h3 class="m-b-0 m-t-10">  مجموع الجملة للمنتجات الناجحة //  {{$totalrealprice}} </h3>

                                        <p class="m-b-0">نسبة المسوق %{{$order->user->userprice}}</p>
                                        <p class="m-b-0">  ربح المسوق من المنتجات الناجحة {{$order->details->where('status','success')->sum('profit')}}</p>


                                </div>
                                <div class="hidden-print col-md-12 text-right mt-4">
                                <button type="submit" class="btn btn-primary">إضافة حالة التسليم</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
          <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="mail-inbox">
                            <div class="mobile-left">
                                <a href="javascript:void(0);" class="btn btn-primary toggle-email-nav"><i class="fa fa-bars"></i></a>
                            </div>

                            <div class="body mail-right check-all-parent">
                                <div class="mail-compose">

                                    <div class="summernote">
                                     اسم الزبون //  {{$order->name}}
                                        <br/>
                                          الإمارة  //   {{$order->emara}}
                                        <br/>
                                         المنطقة  //   {{$order->address}}
                                        <br/>
                                         رقم الهاتف  //   {{$order->mobile}}
                                        <br/>
                                         رقم الواتسآب  //   {{$order->whats}}
                                        <br/>
                                        <br/>
                                         عدد الطلبيات  //   {{$order->details_count}}
                                         <br>

                                     <hr>
                                     <br>
                                        @foreach ($order->details as $item)

                                        اسم القطعة //  {{$item->product->name}}
                                        <br/>
                                          الكود  //   {{$item->product->code}}
                                        <br/>
                                         سعر بيع الجملة  للقطعة   //   {{$item->product->realprice}}
                                        <br/>
                                        سعر البيع للقطعة الواحدة //   {{$item->currentPrice}}
                                        <br/>
                                          العدد  //   {{$item->count}}
                                        <br/>
                                         المجموع  //   {{$item->total}}
                                        <br/>

                                        <br>


                                        @endforeach
                                        <hr>
                                     <p><strong>التحصيل شامل التوصيل:</strong>{{$order->details->sum('total')}} </p>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('script')
    <script>
    $('.toggle-email-nav').on('click', function() {
		$('.mail-left').toggleClass('open');
	});
</script>
@show
