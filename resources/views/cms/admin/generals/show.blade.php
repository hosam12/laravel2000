@extends('cms.admin.parent')
@section('title','الإعدادات العامة')
@section('content')

  <div id="main-content">
            <div class="container">
                <div class="block-header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <h2>User Profile</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                                    <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">الإعدادات العامة</li>
                                </ol>
                            </nav>
                        </div>

                    </div>
                </div>
                <div class="row clearfix">



                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                                <h2 style="font-size: 20px">الصلاحيات العامة</h2>

                            </div>

                        <form action="{{route('general.update',[$general->id])}}" method="POST">
                            @csrf
                            @method("PUT")
                            <div class="body" style="font-size: 20px">
                                <div class="row clearfix">

                                    <div class="col-12">
                                        <ul class="list-group mb-3 tp-setting">
                                            <li class="list-group-item">
                                                رفع الطلبيات
                                                <div class="float-right">
                                                    <label class="switch">
                                                    <input name="create_order" id="create_order" type="checkbox" @if($general->create_order=='Visible') checked @endif >
                                                    <span class="slider round"></span>
                                                </label>
                                                </div>
                                            </li>
                                              <li class="list-group-item">
                                                مشاهدة الطلبيات
                                                    <div class="float-right">
                                                    <label class="switch">
                                                    <input name="index_order" id="index_order" type="checkbox" @if($general->index_order=='Visible') checked @endif >
                                                    <span class="slider round"></span>
                                                </label>
                                                </div>
                                            </li>
                                              <li class="list-group-item">
                                                مشاهدة المنتجات
                                                <div class="float-right">
                                                    <label class="switch">
                                                    <input name="product" id="product" type="checkbox" @if($general->product=='Visible') checked @endif >
                                                    <span class="slider round"></span>
                                                </label>
                                                </div>
                                            </li>
                                              <li class="list-group-item">
                                                مشاهدة الفئات
                                                <div class="float-right">
                                                    <label class="switch">
                                                    <input name="category" id="category" type="checkbox" @if($general->category=='Visible') checked @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                                </div>
                                            </li>
                                               <li class="list-group-item">
                                                 التواصل معنا
                                                <div class="float-right">
                                                    <label class="switch">
                                                    <input name="vedio" id="vedio" type="checkbox" @if($general->vedio=='Visible') checked @endif>
                                                    <span class="slider round"></span>
                                                </label>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-round btn-primary">تعديل</button> &nbsp;&nbsp;
                                 <a href="{{route('admin.dashbord')}}" class="btn btn-round btn-default">رجوع</a>
                            </div>
                        </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

@endsection
