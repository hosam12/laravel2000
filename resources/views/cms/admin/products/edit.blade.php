@extends('cms.admin.parent')
@section('title','تعديل منتج')
@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1 style="font-size: 20px">تعديل منتج</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="{{route('product.index')}}" style="font-size: 20px">المنتجات</a></li>
                            <li class="breadcrumb-item active" aria-current="page"style="font-size: 20px">تعديل منتج</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                       {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                        <form method="POST" action="{{route('product.update',[$product->id])}}" enctype="multipart/form-data">


                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                            @csrf
                            @method("PUT")
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">

                                        <div class="form-group">
                                            <label style="font-size: 20px" >إسم المنتج</label>
                                            <input type="text" class="form-control" value="{{$product->name}}" style="font-size: 20px" name="name" placeholder=" اسم المنتج ">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >كود المنتج</label>
                                            <input type="text" class="form-control" value="{{$product->code}}" style="font-size: 20px" name="code"  placeholder="كود المنتج">
                                        </div>
                                <div class="form-group">
                                            <label style="font-size: 20px">فئة المنتج</label>
                                             <div class="col-md-12 col-sm-10">
                                             <select class="form-control input-height" name="category" >
                                            <option value="">Select...</option>
                                           @foreach ($cate as $item)
                                        <option style="font-size: 20px" @if($product->category_id==$item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>

                                           @endforeach
                                        </select>
                                    </div>
                                </div>
                                    <div class="col-lg-12 col-md-12">
                                        <label style="font-size: 20px" > وصف المنتج </label>

                                        <div class="form-group">
                                        <textarea rows="2" type="text" name="desc" style="font-size: 20px"  id="desc" class="form-control" placeholder=" الوصف العام عن المنتج">{{$product->desc}}</textarea>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                            <label style="font-size: 20px" >سعر البيع من التاجر</label>
                                            <input type="number" class="form-control" value="{{$product->realprice}}" style="font-size: 20px" name="realprice"  placeholder="السعر بالجملة">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >الحد الأدنى</label>
                                            <input type="number" class="form-control" value="{{$product->from}}" style="font-size: 20px" name="from" placeholder="من">
                                        </div>

                                        <div class="form-group">
                                            <label style="font-size: 20px" > الحد الأقصى</label>
                                            <input type="number" class="form-control" value="{{$product->to}}" style="font-size: 20px" name="to" placeholder="إلى">
                                        </div>

                                           <div class="form-group"  >
                                            <label style="font-size: 20px" >   السعر الثابت</label>
                                        <input type="number" class="form-control" value="{{$product->staticprice}}" style="font-size: 20px" name="staticprice" placeholder="السعر الثابت">
                                        </div>




                                          <div class="form-group">
                                            <label style="font-size: 20px" >الألوان المتوفرة</label>
                                            <input type="text" class="form-control" value="{{$product->color}}" style="font-size: 20px" name="color"  placeholder="الألوان">
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-hover js-basic-example dataTable table-custom spacing5 mb-0">
                                                 <thead>
                                                        <tr>
                                                     <th >#</th>
                                                     <tr>الإعدادات</tr>
                                                        </tr>

                                                    </thead>

                                                      <tbody>

                                                               @foreach ($product->images as $i)
                                                                        <tr>
                                                <td class="w60">
                                            <img src="{{url('images/products/'.$i->image)}}" data-toggle="tooltip" data-placement="top" title="{{$product->name}}"  class="w35 h35 rounded">
                                                </td>
                                                <td>
                                        <a onclick="confirmDelete(this, '{{$i->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a>
                                                <a href="{{route('daw.product',[$i->image])}}"  style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="تنزيل" data-type="confirm"><i class="fa fa-trash-o text-success"> تنزيل </i></a>


                                                </td>
                                    </tr>
                                                                       @endforeach

                                                 </tbody>
                                        </table>
                                        </div>
                                         <div class="row clearfix">
                                            <div class="col-lg-12">
                                                 <label style="font-size: 20px" > صور المنتج</label>
                                                <input type="file" multiple   width="590px" height="390px" class="dropify" name="image[]">
                                            </div>

                                        </div>



                                 <div class="form-group">
                                <label style="font-size: 20px">المنتج متوفر</label>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input"  type="radio" id="Male"
                                        name="gender" value="Male"
                                        @if($product->gender=='Male') checked  @endif>
                                    <label style="font-size: 15px" for="Male" class="custom-control-label">ذكور</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Female"
                                        name="gender" value="Female"
                                        @if($product->gender=='Female') checked @endif>
                                    <label style="font-size: 15px" for="Female" class="custom-control-label">إناث</label>
                                </div>
                                  <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="MaleFemale"
                                        name="gender" value="MaleFemale"
                                        @if($product->gender=='MaleFemale') checked @endif>
                                    <label style="font-size: 15px" for="MaleFemale" class="custom-control-label">الجميع</label>
                                </div>
                            </div>



                            <div class="form-group">
                                <label style="font-size: 20px"> تفعيل المنتج</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="status"
                                        id="status"
                                        @if($product->status == 'Visible') checked @endif>
                                    <label class="custom-control-label" style="font-size: 20px" for="status">مُفعل</label>

                        </div>
                    </div>
                                                        <div class="col-lg-12 col-md-12">
                                        <label style="font-size: 20px" > أيةَ مُلاحظة </label>

                                        <div class="form-group">
                                        <textarea rows="2" type="text" name="note"  id="note" class="form-control" placeholder="ملاحظات أُخرى">{{$product->note}}</textarea>
                                        </div>
                                    </div>

                                    </div>
                                </div>
                                <div>
                                            <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit"> تعديل  </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function confirmDelete(app, id) {
            Swal.fire({
                title: 'هل أنت متأكد ؟',
                text: "هل انت متأكد من حذف الصورة ؟!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes Deleted!'
                // cancelButtonText: 'نعم!'

            }).then((result) => {
                if (result.value) {
                    deleteproduct(app, id)
                }
            })
        }

        function deleteproduct(app, id) {
            axios.delete('/image/' + id)
                .then(function (response) {
                    // handle success (Status Code: 200)
                    console.log(response);
                    console.log(response.data);
                    showMessage(response.data);
                    app.closest('tr').remove();
                })
                .catch(function (error) {
                    // handle error (Status Code: 400)
                    console.log(error);
                    console.log(error.response.data);
                    showMessage(error.response.data);
                })
                .then(function () {
                    // always executed
                });
        }

        function showMessage(data) {
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.title,
                showConfirmButton: false,
                timer: 1500
            })
        }
    </script>

@endsection

