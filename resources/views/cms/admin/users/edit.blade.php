@extends('cms.admin.parent')
@section('title','تعديل مسوق')

@section('content')
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1 style="font-size: 20px">تعديل مسوق</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.dashbord')}}" style="font-size: 20px">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="{{route('user.index')}}" style="font-size: 20px">المسوقين</a></li>
                            <li class="breadcrumb-item"><a href="{{route('user.create')}}" style="font-size: 20px">مسوق جديد</a></li>

                            <li class="breadcrumb-item active" aria-current="page"style="font-size: 20px">تعديل مسوق</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                       {{-- <a href="https://themeforest.net/item/oculux-bootstrap-4x-admin-dashboard-clean-modern-ui-kit/23091507" class="btn btn-sm btn-success" title="Themeforest"><i class="icon-basket"></i> Buy Now</a> --}}
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">
                        <form method="POST" action="{{route('user.update',[$user->id])}}" enctype="multipart/form-data">


                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                            @csrf
                            @method("PUT")
                            <div  style="align-content: center" class="col-lg-2 col-md-12 center text-center">
                        <div class="body text-center">
                        <img class="img-thumbnail rounded-circle" src="{{url('images/users/'.$user->image)}}" alt="{{$user->name}}">
                            <h6 class="mt-3">{{$user->name}}</h6>
                        </div>
                </div>
                                <div class="form-group row">
                                    <div class="col-md-10 col-sm-10">

                                            <div class="form-group">
                                            <label style="font-size: 20px" >  ربح المسوق</label>
                                         <input type="tel" class="form-control" name="mall" style="font-size: 20px" value="{{$user->mall}}" >
                                        </div>
                                          <div class="form-group">
                                            <label style="font-size: 20px" >الخصومات الشهرية</label>
                                         <input type="number" class="form-control" name="dis" style="font-size: 20px" value="{{$user->dis}}" >
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >الإسم</label>
                                        <input type="text" class="form-control" value="{{$user->name}}" style="font-size: 20px" name="name" placeholder=" ادخل الإسم رباعي ">
                                        </div>
                                        <div class="form-group">
                                            <label style="font-size: 20px" >رقم الهوية</label>
                                            <input type="number"  class="form-control" value="{{$user->numberid}}" style="font-size: 20px" name="numberid"  placeholder="أدخل رقم الهوية">
                                        </div>
                                           <div class="form-group">
                                            <label style="font-size: 20px" > الإيميل</label>
                                            <input type="email"  class="form-control" value="{{$user->email}}" style="font-size: 20px" name="email"  placeholder="الإيميل الشخصي">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >رقم الهاتف</label>
                                            <input type="tel" class="form-control" value="{{$user->mobile}}" style="font-size: 20px" name="mobile"  placeholder="أدخل رقم الهاتف">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >رقم الواتسآب</label>
                                            <input type="tel" class="form-control" value="{{$user->now}}" style="font-size: 20px" name="now"  placeholder="أدخل رقم الهاتف">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >العنوان</label>
                                            <input type="text" class="form-control" value="{{$user->adress}}" style="font-size: 20px" name="address" placeholder="ادخل العنوان">
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" > العمر</label>
                                            <input type="number" class="form-control" value="{{$user->age}}" style="font-size: 20px" name="age" placeholder="أدخل العمر">
                                        </div>
                                            <label style="font-size: 20px" > الصورة الشخصية</label>

                                         <div class="row clearfix">
                                            <div class="col-lg-12">
                                                <input type="file" width="590px" height="390px" class="dropify" name="image">
                                            </div>

                                        </div>
                                          <div class="form-group">
                                            <label style="font-size: 20px" > كلمة السر</label>
                                          <input type="text" class="form-control" style="font-size: 20px" value="{{$user->view_password}}" disabled >
                                        </div>
                                         <div class="form-group">
                                            <label style="font-size: 20px" >نسبة المسوق بالمئة</label>
                                         <input type="number" class="form-control" name="userprice" style="font-size: 20px" value="{{$user->userprice}}" >
                                        </div>

                                    <div class="form-group">
                                <label style="font-size: 20px">الجنس</label>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input"  type="radio" id="Male"
                                        name="gender" value="Male"
                                        @if($user->gender== 'Male') checked  @endif>
                                    <label  for="Male" class="custom-control-label" style="font-size: 20px">ذكر</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Female"
                                        name="gender" value="Female"
                                        @if($user->gender == 'Female') checked @endif>
                                    <label for="Female" class="custom-control-label" style="font-size: 20px">أنثى</label>
                                </div>
                            </div>
                             <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="font-size: 20px">الصلاحيات</h2>

                        </div>
                        <div class="body">
                            <div class="row clearfix">

                                <div class="col-12">
                                    <ul class="list-group mb-3 tp-setting" style="font-size: 20px">
                                        <li class="list-group-item">
                                                    رفع طلب
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input name="create_order" id="create_order" type="checkbox" @if($user->setting->create_order == 'Visible')checked @endif) >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                                    مشاهدة الطلبيات
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input  name="index_order" @if($user->setting->index_order == 'Visible') checked @endif  id="index_order" type="checkbox">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                                مشاهدة المنتجات
                                            <div class="float-right">
                                                <label class="switch">
                                                    <input name="product" id="product" @if($user->setting->product == 'Visible') checked @endif type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                        <li class="list-group-item">

                                         مشاهدة الفئات
                                                     <div class="float-right">
                                                <label class="switch">
                                                    <input name="category" id="category" @if($user->setting->category == 'Visible') checked @endif type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>
                                                                      <li class="list-group-item">

                                         التواصل معنا
                                                     <div class="float-right">
                                                <label class="switch">
                                                    <input name="vedio" id="vedio" @if($user->setting->vedio == 'Visible') checked @endif type="checkbox" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                            <div class="form-group">
                                <label style="font-size: 20px">حالة الحساب</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="status"
                                        id="status"
                                        @if($user->status == 'Active') checked @endif>
                                    <label class="custom-control-label" style="font-size: 20px" for="status">مُفعل</label>

                        </div>
                    </div>

                                    </div>
                                </div>
                                <div>
                                            <button style="font-size: 20px" class="btn btn-sm btn-primary" type="submit"> تعديل  </button>


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
