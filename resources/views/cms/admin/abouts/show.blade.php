@extends('cms.admin.parent')
@section('title','حول الشركة')
@section('content')
 <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h2>حول الشركة</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('admin.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('admin.show',[Auth::user()->id])}}">الصفحة الشخصية</a></li>
                            <li class="breadcrumb-item active"  style="font-size: 20px" aria-current="page">حول الشركة</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{route('about.edit',[$about->id])}}" class="btn btn-sm btn-primary btn-round" title="">تعديل</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="single-blog">
                        <div class="post-thumb">
                        <a href="#"><img style="height: 380px" width="1024pxs" src="{{url('images/abouts/'.$about->image)}}"   class="img-fluid rounded" alt="Techno Band"></a>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="#">Techno Band</a></h2>
                        <p>{{$about->articale}}</p>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
