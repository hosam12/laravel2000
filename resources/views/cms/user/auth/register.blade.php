<!doctype html>
<html lang="en">

<head>
<title>Techno Band |إشتراك</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
<meta name="author" content="GetBootstrap, design by: puffintheme.com">

<link rel="icon" href="{{asset('cms/favicon.ico')}}" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('cms/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('cms/assets/vendor/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('cms/assets/vendor/animate-css/vivify.min.css')}}">

<!-- MAIN CSS -->
<link rel="stylesheet" href="{{asset('cms/html/assets/css/site.min.css')}}">

</head>

<body class="theme-cyan font-montserrat">

<!-- Page Loader -->
{{-- <div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div> --}}

<div class="pattern">
    <span class="red"></span>
    <span class="indigo"></span>
    <span class="blue"></span>
    <span class="green"></span>
    <span class="orange"></span>
</div>
<div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        <div class="auth_brand">
            <a class="navbar-brand" href="javascript:void(0);"><img src="{{asset('cms/assets/images/icon.svg')}}" width="30" height="30" class="d-inline-block align-top mr-2" alt="">Techno Band</a>
        </div>
        <div class="card">
            <div class="body">
                <p class="lead"> مسوق جديد </p>
            <form class="form-auth-small m-t-20" action="{{route('reg.user.post')}}"  method="POST">
                      @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                         @endif
                @csrf

                    <div class="form-group">
                    <input type="name" name="name" value="{{old('name')}}" class="form-control round" placeholder="الإسم رباعي">
                    </div>

                      <div class="form-group">
                        <input type="email" name="email" value="{{old('email')}}"  class="form-control round" placeholder="الإيميل">
                    </div>
                      <div class="form-group">
                        <input type="tel" name="mobile" value="{{old('mobile')}}"  class="form-control round" placeholder="رقم الهاتف">
                    </div>
                    <div class="form-group">
                        <input type="number" name="age" value="{{old('age')}}"  class="form-control round" placeholder="العمر">
                    </div>
                    <div class="form-group">
                        <input type="text" name="adress" value="{{old('adress')}}"  class="form-control round" placeholder="العنوان">
                    </div>
                     <div class="form-group">
                        <input type="password" name="password" class="form-control round" placeholder="كلمة السر">
                    </div>
                     <div class="form-group">
                        <input type="password" name="password2" class="form-control round" placeholder="تأكيد كلمة السر">
                    </div>

                         <div  class="form-group">
                                <label style="font-size: 20px">الجنس</label>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input"  type="radio" id="Male"
                                        name="gender" value="Male"
                                        @if(old('gender') == 'Male') checked  @endif>
                                    <label style="font-size: 15px" for="Male" class="custom-control-label">ذكر</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input class="custom-control-input" type="radio" id="Female"
                                        name="gender" value="Female"
                                        @if(old('gender') == 'Female') checked @endif>
                                    <label style="font-size: 15px" for="Female" class="custom-control-label">أنثى</label>
                                </div>
                            </div>
                    <button type="submit" class="btn btn-primary btn-round btn-block">تسجيل</button>
                </form>
                {{-- <a href="" class="btn btn-success">egsdds</a> --}}
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>
<!-- END WRAPPER -->

<script src="{{asset('html/assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('html/assets/bundles/vendorscripts.bundle.js')}}"></script>
<script src="{{asset('html/assets/bundles/mainscripts.bundle.js')}}"></script>
</body>
</html>
