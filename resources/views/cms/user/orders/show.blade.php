@extends('cms.user.parent')
@section('title','طلبية')
@section('content')


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('order.index')}}">الطلبيات</a></li>
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">طلبية</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="body">
                            <article class="media">

                                <div class="media-body">
                                    <div class="content">

                                        <p>اسم الزبون</p>
                                        <p>{{$order->name}}</p>
                                        <p>رقم الواتس للزبون </p>
                                        <p>{{$order->whats}}</p>
                                        <p> رقم الطلبية // {{$order->id}} </p>

                                    </div>

                                </div>
                            </article>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th> اسم المنتج</th>
                                                    <th>فئة المنتج</th>
                                                    <th>كود المنتج</th>
                                                    <th>سعر الجملة </th>
                                                    <th>سعر بيع القطعة الواحدة</th>
                                                    <th>العدد</th>
                                                    <th style="width: 80px;">المجموع</th>
                                                    <th style="width: 80px;">حالة التسليم</th>

                                                    <th>الإعدادات</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <span hidden>{{$i=0}}</span>
                                                @foreach ($order->details as $details)
                                                   <span hidden>{{$i++}}</span>
                                                      <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$details->product->name}}</td>
                                                            <td>{{$details->product->category->name}}</td>
                                                            <td>{{$details->product->code}}</td>
                                                            <td>{{$details->product->realprice}}</td>
                                                            <td>{{$details->currentPrice}}</td>
                                                            <td>{{$details->count}}</td>
                                                            <td>{{$details->total}}</td>
                                                               <td>
                                         @if($details->status=='ublode'||$details->status=='waiting')
                                        <span style="font-size: 17px" class="badge badge-light">قيد الإعتماد</span>
                                     @elseif($details->status=='cancel'||$details->order->status=='cancel')
                                    <span style="font-size: 17px" class="badge badge-danger">مرفوضة</span>
                                      @elseif($details->status=='success')
                                    <span style="font-size: 17px" class="badge badge-success">تم التسليم</span>




                                        @endif

                                    </td>
                                                            <td>
                                        <a href="{{route('daw.order',[$details->image])}}" class="btn btn-success"> تنزيل صورة المنتج </a>

                                        {{-- <a onclick="confirmDelete(this, '{{$user->id}}')" type="button" style="font-size: 20px" class="btn btn-sm btn-default js-sweetalert" title="حذف" data-type="confirm"><i class="fa fa-trash-o text-danger"> حذف </i></a> --}}


                                                            </td>

                                                </tr>
                                                @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

