@extends('cms.user.parent')
@section('title','المنتجات')


@section('content')
  <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h2>المنتجات</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                                @if (Auth::user()->setting->create_order=='Visible'&&Auth::user()->general->create_order=='Visible')

                            <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('order.create')}}">طلب جديد</a></li>
                        @endif
                            <li class="breadcrumb-item active" style="font-size: 20px" aria-current="page">المنتجات</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                        <a href="{{route('order.create')}}"  style="font-size: 20px" class="btn btn-sm btn-primary btn-round" title="">رفع طلب</a>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                @foreach ($products as $item)
                @if($item->category->status=='Visible')
                    <div class="col-lg-4 cool-md-4 col-sm-6"  >
                    <div class="card">
                        <ul  class="pricing body active" >
                        <li class="plan-img"><img class="img-fluid rounded-circle" src="{{url('images/products/'.$item->images[0]->image)}}" alt="{{$item->name}}" height="150px" width="100px" /></li>
                        <h4>{{Str::limit($item->name, 15)}}</h4>
                        @if ($item->from)
                            <li  class="price">
                                  <span>الحد الأدنى</span >
                                <h3 > {{$item->from}} </h3>
                                  <span>الحد الأعلى</span>
                                <h3> {{$item->to}} </h3>
                            </li>
                             @endif

                             @if ($item->staticprice)
                                 <li class="price">
                                  <span> السعر ثابت</span>
                                <h3> {{$item->staticprice}} </h3>

                            </li>
                             @endif
                            <li>
                                 <span>كود المنتج</span><br>
                                <h3 style="color: red">{{$item->code}}</h3>
                            </li>


                            <li>
                                 <span> الفئة</span><br>
                                <strong>{{$item->category->name}}</strong>
                            </li>
                        <li class="plan-btn">
                            <a href="{{route('image.show',[$item->id])}}" class="btn btn-round btn-primary">تصفح الصور</a>
                            <a href="{{route('daw.product',[$item->images[0]->image])}}" class="btn btn-round btn-danger">تنزيل الصورة</a>


                        </li>
                        </ul>
                    </div>
                </div>
                @endif
                @endforeach


            </div>
        </div>
    </div>
@endsection
