@extends('cms.user.parent')
@section('title','مسوق')
@section('content')
     <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h1 style="font-size: 20px">الرئيسية</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            @if ((Auth::user()->setting->index_order=='Visible'&&Auth::user()->general->index_order=='Visible')||(Auth::user()->setting->create_order=='Visible'&&Auth::user()->general->create_order=='Visible'))

                            <li class="breadcrumb-item"><a href="{{route('order.index')}}" style="font-size: 20px">طلباتي</a></li>
                            @endif
                            <li class="breadcrumb-item active" aria-current="page" style="font-size: 20px">الرئيسية</li>
                            </ol>
                        </nav>
                    </div>
                             @if (Auth::user()->setting->create_order=='Visible'&&Auth::user()->general->create_order=='Visible')
                    <div class="col-md-6 col-sm-12 text-right hidden-xs">
                    <a href="{{route('order.create')}}" class="btn btn-sm btn-primary" style="font-size: 20px" title="">رفع طلب</a>
                    </div>
                    @endif
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-indigo text-white rounded-circle"><i class="fa fa-briefcase"></i></div>
                                <div class="ml-4">
                                    <span style="font-size: 20px">مجموع المبيعات</span>
                                <h4 class="mb-0 font-weight-medium">{{$details->where('status','success')->sum('total')}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">

                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-azura text-white rounded-circle"><i class="fa fa-credit-card"></i></div>
                                <div class="ml-4">
                                    <span> الطلبات  الناجحة</span>
                                <h4 class="mb-0 font-weight-medium">{{$details->where('status','success')->count()}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-orange text-white rounded-circle"><i class="fa fa-users"></i></div>
                                <div class="ml-4">
                                    <span>الأرباح</span>

                                <h4 class="mb-0 font-weight-medium">{{$profit}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-azura text-white rounded-circle"><i class="fa fa-credit-card"></i></div>
                                <div class="ml-4">
                                    <span> المحفظة</span>
                                <h4 class="mb-0 font-weight-medium">{{Auth::user()->mall-Auth::user()->dis}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-pink text-white rounded-circle"><i class="fa fa-life-ring"></i></div>
                                <div class="ml-4">
                                    <span>النسبة المئوية</span>
                                <h4 class="mb-0 font-weight-medium">{{Auth::user()->userprice}} % </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                            <div class="icon-in-bg bg-orange text-white rounded-circle"><a href="{{route('user.month',[Auth::user()->id])}}"><i class="fa fa-life-ring"></i></a></div>
                                <div class="ml-4">
                                    <span> طلبات الشهر</span>
                                <h4 class="mb-0 font-weight-medium">{{$month->count()}} </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                                <div class="icon-in-bg bg-red text-white rounded-circle"><a href="{{route('ser.index')}}"><i class="icon-star"></a></i></div>
                                <div class="ml-4">
                                    <span>  أخبار ومميزات الشركة</span>
                                {{-- <h4 class="mb-0 font-weight-medium">{{Auth::user()->mall-Auth::user()->dis}}</h4> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="body">
                            <div class="d-flex align-items-center">
                            <div class="icon-in-bg bg-green text-white rounded-circle"><i class="fa fa-life-ring"></i></div>
                                <div class="ml-4">
                                    <span> الخصومات الشهرية</span>
                                <h4 class="mb-0 font-weight-medium">{{Auth::user()->dis}} </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>الإحصائية</h2>
                        </div>
                        <div class="body">
                            <div class="row text-center">
                                <div class="col-6 border-right pb-4 pt-4">
                                    <label class="mb-0">المنتجات المُباعة</label>
                                <h4 class="font-30 font-weight-bold text-col-blue">{{$details->where('status','success')->count()}}</h4>
                                </div>
                                <div class="col-6 pb-4 pt-4">
                                    <label class="mb-0">المنتجات المرجعة</label>
                                    <h4 class="font-30 font-weight-bold text-col-blue">{{$details->where('status','cancel')->count()}}</h4>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="col-lg-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom spacing5">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">رقم الطلبية</th>
                                    <th>الزبون</th>
                                    <th style="width: 50px;">المبلغ</th>
                                    <th style="width: 50px;">الحالة</th>
                                    {{-- <th style="width: 110px;">Action</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $item)
                                     <tr>
                                    <td>
                                    <span>{{$item->id}}</span>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <div class="ml-3">
                                            <a href="{{route('order.show',[$item->id])}}" title="">{{$item->name}}</a>
                                            </div>
                                        </div>
                                    </td>
                                <td>{{$item->details->sum('total')}}</td>
                                @if ($item->status=='waiting'||$item->status=='ublode')
                                <td><span class="badge badge-warning ml-0 mr-0">قيد الإعتماد</span></td>

                                @elseif($item->status=='success')
                                <td><span class="badge badge-success ml-0 mr-0">نجاح</span></td>
                                    @elseif($item->status=='cancel')
                                     <td><span class="badge badge-danger ml-0 mr-0">مرفوضة</span></td>

                                @endif

                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>


            </div>


        </div>
    </div>
@endsection
