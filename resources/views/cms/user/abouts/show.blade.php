@extends('cms.user.parent')
@section('title','Techno Band')
@section('content')
 <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12">
                        <h2>حول الشركة</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.profile')}}">الصفحة الشخصية</a></li>
                            <li class="breadcrumb-item active"  style="font-size: 20px" aria-current="page">حول الشركة</li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="single-blog">
                        <div class="post-thumb">
                        <a href="#"><img  style="height: 380px" width="1024px" src="{{url('images/abouts/'.$about->image)}}" class="img-fluid rounded" alt="Techno Band"></a>
                        </div>
                        <div class="post-content overflow">
                            <h2 class="post-title bold"><a href="#">Techno Band</a></h2>
                        <p>{{$about->articale}}</p>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

