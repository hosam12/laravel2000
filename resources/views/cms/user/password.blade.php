@extends('cms.user.parent')
@section('title','تغيير كلمة السر')
@section('content')
   <div id="main-content">
            <div class="container">
                <div class="block-header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <h2>تغيير كلمة السر</h2>
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a style="font-size: 20px" href="{{route('user.profile',[Auth::user()->id])}}">الصفحة الشخصية</a></li>
                                    <li class="breadcrumb-item"><a  style="font-size: 20px" href="{{route('user.dashbord')}}">الرئيسية</a></li>
                                    <li class="breadcrumb-item active" style="font-size: 20px"  aria-current="page">تغيير كلمة السر</li>
                                </ol>
                            </nav>
                        </div>

                    </div>
                </div>
                <div class="row clearfix">



                    <div class="col-xl-8 col-lg-8 col-md-7">

                        <div class="card">
                        <form action="{{route('user.password',[Auth::user()->id])}}" method="POST">
                              @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </div>
                        @endif
                        @if (session()->has('message'))
                                        <div class="alert {{session()->get('status')}} alert-dismissible fade show"
                                             role="alert">
                                            <span> {{ session()->get('message') }}</span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                         @endif
                            @csrf
                            @method("PUT")
                            <div class="body">
                                <div class="row clearfix">


                                    <div class="col-lg-12 col-md-12">
                                        <hr>
                                        <h6> تغيير كلمة السر</h6>
                                        <div class="form-group">
                                            <input type="password" name="cuurent_password" class="form-control" placeholder="كلمة  السر الحالية ">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="newpassword" class="form-control" placeholder="كلمة السر الجديدة">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="confairm_password" class="form-control" placeholder="تأكيد كلمة السر الجديدة">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-round btn-primary">تعديل</button> &nbsp;&nbsp;
                            <a href="{{route('user.dashbord')}}" type="button" class="btn btn-round btn-default">رجوع</a>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
