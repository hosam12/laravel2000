<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Monolog\Handler\RotatingFileHandler;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Auth::routes();

//************************************************//
//****************** CMS-ADMIN *******************//
//************************************************//

// Route::view('/', 'cms.admin.parent')->name('admin.blocked');

Route::prefix('cms/admin/')->middleware('auth:admin')->group(function () {

    Route::get('noti', function () {
    $user=Auth::user();
    foreach($user->unreadNotifications as $item){
    $item->markAsRead();
    }
    });



Route::resource('admin', 'AdminController');
Route::put('status/{id}','OrderController@editstatus')->name('status.order');
Route::resource('general', 'GeneralController');
Route::put('/changepaswword/{id}', 'AdminController@password')->name('admin.password');
Route::get('user/details/{details}','UserController@userdetails')->name('user.details');
Route::get('/serves/all','ServeController@all')->name('ser.all');

Route::resource('user', 'UserController');
Route::resource('setting', 'SettingController');
Route::resource('comp', 'CompanyController');
// Route::get('comp/all','DashController@all')->name('comp.all');

Route::resource('product', 'ProductController');
Route::get('','dashController@admin')->name('admin.dashbord');
Route::get('/ublode','dashController@orderublode')->name('order.ublode');
Route::get('logout', 'Auth\AdminAuthController@logout')->name('admin.logout');
Route::get('/selectuser/{id}', 'ContactController@selectuser')->name('select.user');



});
//************************************************//
//****************** CMS-ADMIN----User *******************//
//************************************************//
Route::get('/user/month/{file}','dashController@usermonth')->middleware('auth:admin,user')->name('user.month');
Route::resource('category', 'CategoryController')->middleware('auth:admin,user');
Route::get('/month/details','dashController@detailsmonth')->middleware('auth:admin')->name('details.month');
// Route::get('/month/details','dashController@detailsmonth')->middleware('auth:admin,user')->name('details.month');
Route::get('/categoryproducts/{id}','CategoryController@CategoryProduct')->middleware('auth:admin,user')->name('category.product');
Route::post('details/data','OrderProductController@detailsdata')->middleware('auth:admin,user')->name('details.data');
Route::resource('details', 'orderProductController')->middleware('auth:admin,user');
Route::get('downloadorder/{file}','ImageController@downloadorder')->middleware('auth:admin,user')->name('daw.order');
Route::Post('searchAdmin','DashController@searchadmin')->middleware('auth:admin')->name('search.admin');

Route::resource('product', 'ProductController')->middleware('auth:admin,user');
Route::resource('ser', 'ServeController')->middleware('auth:admin,user');

Route::get('downloadproduct/{file}','ImageController@downloadproduct')->middleware('auth:admin,user')->name('daw.product');
Route::get('contactresiver/','ContactController@contactresever')->middleware('auth:admin,user')->name('contact.resiver');


Route::resource('order', 'OrderController')->middleware('auth:admin,user');
Route::resource('image', 'ImageController')->middleware('auth:admin,user');
Route::resource('about', 'AboutController')->middleware('auth:admin,user');
Route::resource('contact', 'ContactController')->middleware('auth:admin,user');

////////////////////////////





Route::prefix('cms/admin/')->namespace('Auth')->group(function () {
Route::get('login', 'AdminAuthController@showLoginView')->name('admin.login.view');
Route::post('login', 'AdminAuthController@login')->name('admin.login.post');
Route::view('blocked', 'cms.admin.blocked')->name('cms.admin.blocked');
});


//************************************************//
//****************** CMS-User *******************//
//************************************************//
Route::prefix('cms/user/')->middleware('auth:user')->group(function () {


 Route::get('noti', function () {
    $user=Auth::user();
    foreach($user->unreadNotifications as $item){
    $item->markAsRead();
       }
       });
Route::get('about','AboutController@userabout')->name('user.about');
Route::Post('searchAdmin','DashController@searchadmin')->middleware('auth:admin')->name('search.admin');
Route::Post('searchuser','DashController@searchuser')->middleware('auth:user')->name('search.user');

Route::view('/create', 'cms.user.orders.create');
Route::put('/changepaswword/{id}', 'UserController@password')->name('user.password');
Route::view('/changepassword','cms.user.password')->name('user.view.changepassword');
Route::get('','dashController@user')->name('user.dashbord');
Route::get('logout', 'Auth\UserAuthController@logout')->name('user.logout');
Route::get('user/profile','DashController@userprofile')->name('user.profile');
Route::put('user/edit/user/{file}','DashController@edituser')->name('edit.profile');
Route::view('/editprfile','cms.user.editprofile')->name('edit.user.view');
    });


Route::prefix('cms/user/')->namespace('Auth')->group(function () {



Route::get('login', 'UserAuthController@showLoginView')->name('user.login.view');
Route::post('login', 'UserAuthController@login')->name('user.login.post');
Route::view('blocked', 'cms.user.blocked')->name('user.blocked');
Route::get('/register','RegisterControllerUser@reguser')->name('reg.user.view');
Route::post('/post/register','RegisterControllerUser@reguserpost')->name('reg.user.post');

 });

Route::post('/cms/user/searchorder','ProductController@getcode')->name('get.come')->middleware('auth:user');
// Route::post('/user/test','dashController@test')->name('user.test')->middleware('auth:user');
//////////////////////////////////////////////////////////////////////////
Route::get('','WebController@cms')->name('web.index');

Route::get('/comp/all', 'CompanyController@all')->middleware('auth:admin')->name('comp.all');

