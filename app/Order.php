<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    //

      public function product(){

      return $this->belongsToMany(Product::class,OrderProduct::class,'order_id','product_id');

      }
      public function user(){
          return $this->belongsTo(User::class,'user_id','id');
      }
      public function details(){
          return $this->hasMany(OrderProduct::class,'order_id','id');
      }
}
