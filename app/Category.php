<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Category extends Model
{
    //
        use SearchableTrait;

    use SoftDeletes;

    /**
    * Searchable rules.
    *
    * @var array
    */
    protected $searchable = [
    /**
    * Columns and their priority in search results.
    * Columns with higher values are more important.
    * Columns with equal values have equal importance.
    *
    * @var array
    */
    'columns' => [
    'categories.name' => 10,
    // 'products.name' => 10,
    // 'products.code' => 2,

    ],
    // 'joins' => [
    // 'products' => ['categories.id', 'products.category_id'],
    // ],
    ];


    public function product(){
        return $this->hasMany(Product::class,'category_id','id');
    }
    public function images(){
    return $this->morphToMany(Image::class,'object');
    }
}
