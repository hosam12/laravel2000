<?php

namespace App;
use Nicolaslopezj\Searchable\SearchableTrait;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use SearchableTrait;

    use Notifiable;

    /**
    * Searchable rules.
    *
    * @var array
    */
    protected $searchable = [
    /**
    * Columns and their priority in search results.
    * Columns with higher values are more important.
    * Columns with equal values have equal importance.
    *
    * @var array
    */
    'columns' => [
    'users.name' => 10,
    'users.numberid' => 10,
    'users.adress' => 2,
    'users.email' => 5,

    ],

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
       public function admin()
       {
       return $this->belongsTo(Admin::class, 'admin_id', 'id');
       }

       public function setting(){
           return $this->hasOne(Setting::class,'user_id','id');
       }
       public function general(){
           return $this->belongsTo(General::class,'general_id','id');
       }
       public function order(){
           return $this->hasMany(Order::class,'user_id','id');
       }
        public function details(){
        return $this->hasManyThrough(OrderProduct::class,Order::class,'user_id','order_id');


        }
          public function contact(){
          return $this->hasMany(Contact::class,'admin_id','id');
          }
}
