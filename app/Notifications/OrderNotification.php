<?php

namespace App\Notifications;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderNotification extends Notification
{
                    use Queueable;
                private $order;
                private $name;
                public function __construct(Order $order,$name)
                {
                $this->order=$order;
                $this->name=$name;

                }

                public function via($notifiable)
                {
                return ['database'];
                }


                public function toDatabase(){

                return[
                'id'=>$this->order->id,
                'title'=>$this->name,
                'type'=>'order',
                'image'=>$this->order->user->image,
                'emara'=>$this->order->emara,

                'data'=>$this->order->created_at,
                ];
                }

                /**
                * Get the array representation of the notification.
                *
                * @param mixed $notifiable
                * @return array
                */
                public function toArray($notifiable)
                {
                return [
                //
                ];
                }
   }
