<?php

namespace App\Notifications;

use App\Admin;
use App\contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class ContactNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
      private $contact;
      private $name;
    public function __construct(Contact $contact,$name)
    {
        //
          $this->contact=$contact;
          $this->name=$name;
    }



    public function via($notifiable)
    {
        return ['database'];
    }


     public function toDatabase(){

     return[
     'id'=>$this->contact->id,
     'title'=>$this->name,
     'from'=>Auth::user()->name,
     'type'=>'contact',
     'data'=>$this->contact->created_at,
     ];
     }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
