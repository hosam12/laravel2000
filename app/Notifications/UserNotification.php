<?php

namespace App\Notifications;

use App\user;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class userNotification extends Notification
{
    use Queueable;

    private $user;
    private $name;
    public function __construct(User $user,$name)
    {
        $this->user=$user;
           $this->name=$name;

    }

    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase(){

        return[
      'id'=>$this->user->id,
      'title'=>$this->name,
      'type'=>'user',
      'data'=>$this->user->created_at,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
