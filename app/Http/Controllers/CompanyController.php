<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          if(Auth::guard('admin')->check()){


          return view('cms.admin.comp.index',['images'=>Company::all()]);
          }
              else
              return redirect()->route('user.dashbord');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

               if(Auth::guard('admin')->check()){
               return view('cms.admin.comp.create');
               }
                   else
                   return redirect()->route('user.dashbord');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
              if(Auth::guard('admin')->check()){

              $request->validate([
              'name'=>'required|string|min:5',
              'image'=>'required|image',
              'status'=>'in:on',

              ],[
              'name.required'=>'العنوان مطلوب',
              'name.min'=>'العنوان أفل من 5 حروف',
              'image.image'=>'يجب أن يكون الملف عبارة عن صورة'
              ]);
              $comp=new Company();
              $comp->name=$request->get('name');
              $comp->status=$request->get('status')=='on'?'Visible':'InVisible';
              if($request->hasFile('image')){
              $imagefile=$request->file('image');
              $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

              $imagefile->move('images/companys',$imagename);
              $comp->image=$imagename ;
              }
              $save=$comp->save();
              if($save){
              Alert::success('تم الإنشاء بنجاح', 'نجحت العملية');
              return redirect()->back();
              }
              }
                  else
                  return redirect()->route('user.dashbord');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
            if(Auth::guard('admin')->check()){

            return view('cms.admin.comp.edit',['comp'=>Company::find($id)]);
            }

                else
                return redirect()->route('user.dashbord');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          if(Auth::guard('admin')->check()){


          $request->validate([
          'name'=>'required|string|min:5',
          'status'=>'in:on',

          ],[
          'name.required'=>'العنوان مطلوب',
          'name.min'=>'العنوان أفل من 5 حروف',
          ]);
          $comp=Company::findOrFail($id);
          $comp->name=$request->get('name');
          $comp->status=$request->get('status')=='on'?'Visible':'InVisible';
          if($request->hasFile('image')){
          $imagefile=$request->file('image');
          $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

          $imagefile->move('images/companys',$imagename);
          $comp->image=$imagename ;
          }
          $save=$comp->save();
          if($save){
          Alert::success('تم التعديل بنجاح', 'نجحت العملية');
          return redirect()->back();
          }}
              else
              return redirect()->route('user.dashbord');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Auth::guard('admin')->check()){


         $del=Company::destroy($id);

         if ($del){
         return response()->json(['icon'=>'success','title'=>'تم الحذف بنجاح '],200);
         }else{
         return response()->json(['icon'=>'error','title'=>'Deleted Post failed'],400);
         }}
             else
             return redirect()->route('user.dashbord');
    }


public function all(){
            $comps=Company::all();

    return view('cms.admin.comp.all',['comps'=>$comps]);
}
}
