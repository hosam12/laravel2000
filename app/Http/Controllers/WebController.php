<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Company;
use Illuminate\Http\Request;

class WebController extends Controller
{
 public function cms(){

    $admins=Admin::where('status','Active')->get();
    $comp=Company::where('status','Visible')->paginate(4);
    return view('web.index',['admins'=>$admins,'companys'=>$comp]);
 }
}
