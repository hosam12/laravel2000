<?php

namespace App\Http\Controllers\Auth;

use App\Admin;
use App\General;
use App\Http\Controllers\Controller;
use App\Notifications\userNotification;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterControllerUser extends Controller
{
    //

     public function __construct()
     {

     $this->middleware('guest:user');
     }
    public function reguser(){
        return view('cms.user.auth.register');
    }
      public function reguserpost(Request $request){
        //    dd(33);
         $request->validate([
         'name'=>'required|string|min:10|max:45',
         'email'=>'required|email|unique:users,email|unique:admins,email',
         'mobile'=>'required|numeric|unique:users,mobile',
         'adress'=>'required|string|min:10|max:45',
         'age'=>'required|integer|min:18',
         'password'=>'required|string|min:8',
         'gender'=>'required|string|in:Male,Female',



         ],[

         'name.required'=>'الاسم مطلوب',
         'mobile.required'=>'الهاتف مطلوب',
         'mobile.unique'=>'تم استخدام هذا الرقم من قبل',
         'name.min'=>'أحرف الاسم أقل من 10 ',
         'name.max'=>'أحرف الاسم أكبر من 45 ',
         'email.required'=>'الإيميل مطلوب',
         'email.email'=>'الإيميل خاطئ',
         'email.unique'=>'هذا الحساب مسجل من قبل ',
         'adress.required'=>'العنوان مطلوب',
         'adress.min'=>'أحرف العنوان أقل من 10',
         'adress.max'=>'أحرف العنوان أكبر من 45 ',
         'age.required'=>'العمر مطلوب',
         'age.min'=>'يجب أن يتجاوز المسوق 16 عام ',
         'password.required'=>'كلمة السر مطلوبة',
         'password.min'=>'كلمة السر يجب أن تتجاوز 8 حروف',
         'gender.required'=>'الرجاء أختيار الجنس',
         ]);
         if($request->password){
         $request->validate([
         'password2'=>'required|string|min:8|same:password',
         ],[

         'password.required'=>'تأكيد كلمة السر مطلوب',
         'password2.same'=>'كلمة السر ليست متطابقة',

         ]);
         }


         $user=new User();
         $user->name=$request->get('name');
         $user->mobile=$request->get('mobile');
         $user->email=$request->get('email');
         $user->adress=$request->get('adress');
         $user->age=$request->get('age');
         $user->image='1593959429 حسام الدين ماهر اعطا ابو سكران  user.png';
         $user->userprice=40;

         $user->general_id=General::first()->id;
         $user->view_password=$request->get('password');
         $user->password=Hash::make($request->password);
         $user->gender=$request->get('gender');

         $save= $user->save();
         if($save){
              $admins=Admin::all();
              $name='مستخدم جديد';
              $note=new userNotification($user,$name);
              for ($i=0; $i <count($admins) ; $i++) { $admins[$i]->notify($note);
                  }
    $setting=new Setting();
    $setting->create_order='InVisible';
    $setting->index_order='InVisible';
    $setting->product='Visible';
    $setting->category='Visible';
    $setting->vedio="Visible";
    $setting->user_id=$user->id;
    $setSave= $setting->save();


         }



         if($setSave){

                     $credentials = [
                     'email' => $request->email,
                     'password' => $request->password,
                     ];

                     if (Auth::guard('user')->attempt($credentials, false)) {
                     $user = Auth::guard('user')->user();
                     if ($user->status == "Active") {

                    Alert::toast('أهلاً وسهلاً بك في عائلة Techno Band', 'success');
                     return redirect()->route('user.dashbord');
                     } else {
                     Auth::guard('user')->logout();
                     return redirect()->guest(route('user.blocked'));
                     }
                     } else {
                     return redirect()->back()->withInput();
                     }

         }

    //   return view('cms.user.auth.register');
      }

}
