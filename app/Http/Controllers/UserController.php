<?php

namespace App\Http\Controllers;

use App\General;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users=User::withCount(['details'])->get();
        return view('cms.admin.users.index',['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cms.admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'name'=>'required|string|min:10|max:45',
            'numberid'=>'required|integer|min:100000000|max:999999999|unique:users,numberid',
            'email'=>'required|email|unique:users,email|unique:admins,email',
            'mobile'=>'required|numeric|unique:users,mobile',
            'image'=>'required|image',
            'address'=>'required|string|min:10|max:45',
            'age'=>'required|integer|min:18',
            'password'=>'required|string|min:8',
            'gender'=>'required|string|in:Male,Female',
            'status'=>'string|in:on',
            'userprice'=>'integer',



        ],[

            'name.required'=>'الاسم مطلوب',
            'mobile.required'=>'الهاتف مطلوب',
            'mobile.unique'=>'تم استخدام هذا الرقم من قبل',
            'name.min'=>'أحرف الاسم أقل من 10 ',
            'name.max'=>'أحرف الاسم أكبر من 45 ',
            'numberid.required'=>'رقم الهوية مطلوب',
            'numberid.min'=>'رقم الهوية يجب أن يكون 9 أرقام',
            'numberid.max'=>'رقم الهوية يجب أن يكون 9 أرقام',
            'image.required'=>'الرجاء رفع صورة',
            'image.image'=>'يجب أن تكون صورة',
            'numberid.unique'=>'هذا الحساب مسجل من قبل ',
            'email.required'=>'الإيميل مطلوب',
            'email.email'=>'الإيميل خاطئ',
            'email.unique'=>'هذا الحساب مسجل من قبل ',
            'address.required'=>'العنوان مطلوب',
            'address.min'=>'أحرف العنوان أقل من 10',
            'address.max'=>'أحرف العنوان أكبر من 45 ',
            'age.required'=>'العمر مطلوب',
            'age.min'=>'يجب أن يتجاوز المسوق 16 عام ',
            'password.required'=>'كلمة السر مطلوبة',
            'password.min'=>'كلمة السر يجب أن تتجاوز 8 حروف',
            'gender.required'=>'الرجاء أختيار الجنس',
            'userprice.integer'=>'نسبة المسوق يجب أن تكون رقم '
        ]);
        if($request->password){
            $request->validate([
               'password2'=>'required|string|min:8|same:password',
            ],[

                'password.required'=>'تأكيد كلمة السر مطلوب',
                'password2.same'=>'كلمة السر ليست متطابقة',

            ]);
        }


        $general=General::first();
        $user=new User();
        $user->name=$request->get('name');
        $user->mobile=$request->get('mobile');
        $user->email=$request->get('email');
        $user->numberid=$request->get('numberid');
        $user->adress=$request->get('address');
        $user->age=$request->get('age');
        $user->userprice=$request->get('userprice');

        $user->general_id=$general->id;
        $user->view_password=$request->get('password');
        $user->password=Hash::make($request->password);
        $user->gender=$request->get('gender');
        $user->admin_id=Auth::user()->id;
       $user->status=$request->get('status')=='on'?'Active':'Blocked';
        if($request->hasFile('image')){
        $imagefile=$request->file('image');
        $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

        $imagefile->move('images/users',$imagename);
        $user->image=$imagename ;
        }
        $save= $user->save();
        $setting=new Setting;
        $setting->create_order=$request->get('create_order')=='on'?'Visible':'InVisible';
        $setting->index_order=$request->get('index_order')=='on'?'Visible':'InVisible';
        $setting->product=$request->get('product')=='on'?'Visible':'InVisible';
        $setting->category=$request->get('category')=='on'?'Visible':'InVisible';
        $setting->vedio=$request->get('vedio')=='on'?'Visible':'InVisible';
        $setting->user_id=$user->id;
        $setSave=  $setting->save();



          if($setSave){
         Alert::success('تم الإنشاء بنجاح', 'نجحت العملية');
         return redirect()->back();
          }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user=User::findOrFail($id);
    return view('cms.admin.users.show',['user'=>$user]);
    }



    public function userdetails($id){
        $details=User::findOrFail($id)->details()->withTrashed()->with(['product'=>function($query){
            $query->withTrashed()->get();
        }])->get();
        return view('cms.admin.details.index',['details'=>$details]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user=User::findOrFail($id);
        return view('cms.admin.users.edit',['user'=>$user]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->numberid);
        //
          $request->validate([
          'name'=>'required|string|min:10|max:45',
            'numberid'=>'required|integer|min:100000000|max:999999999',
            'email'=>'required|email|unique:admins,email',

          'mobile'=>'required|numeric',
          'address'=>'required|string|min:10|max:45',
          'age'=>'required|integer|min:18',
          'gender'=>'required|string|in:Male,Female',
          'status'=>'string|in:on',
          'image'=>'image',
            'userprice'=>'integer',
            'mall'=>'integer',





          ],[

          'name.required'=>'الاسم مطلوب',
          'mobile.required'=>'الهاتف مطلوب',
          'mobile.unique'=>'تم استخدام هذا الرقم من قبل',
            'email.required'=>'الإيميل مطلوب',
            'email.email'=>'الإيميل خاطئ',

          'name.min'=>'أحرف الاسم أقل من 10 ',
          'name.max'=>'أحرف الاسم أكبر من 45 ',
          'image.image'=>'يجب أن تكون صورة',
          'address.required'=>'العنوان مطلوب',
          'address.min'=>'أحرف العنوان أقل من 10',
          'address.max'=>'أحرف العنوان أكبر من 45 ',
          'age.required'=>'العمر مطلوب',
          'age.min'=>'يجب أن يتجاوز المسوق 16 عام ',
          'gender.required'=>'الرجاء أختيار الجنس',
            'userprice.integer'=>'نسبة المسوق يجب أن تكون رقم ',
            'numberid.max'=>'رقم الهوية يجب أن يكون 9 أرقام',
            'numberid.min'=>'رقم الهوية يجب أن يكون 9 أرقام',
            'numberid.required'=>'رقم الهوية مطلوب',
            'mall.integer'=>'يجب أن يكون الربح رقم صحيح وليس كسر '

          ]);

              $e=User::where('id','!=',$id)->get();
              $t=$e->where('numberid',$request->numberid)->first();
              if($t){
              $request->session()->flash('status','alert-danger');
              $request->session()->flash('message','هذا الحساب مسجل من قبل');
              return redirect()->back();

              }
               $e=User::where('id','!=',$id)->get();
               $t=$e->where('email',$request->email)->first();
               if($t){
               $request->session()->flash('status','alert-danger');
               $request->session()->flash('message','هذا الحساب مسجل من قبل');
               return redirect()->back();

               }

          $user=User::findOrFail($id);
          $user->numberid=$request->get('numberid');
          $user->mall=$request->mall;
          $user->dis=$request->get('dis');
          $user->name=$request->get('name');
          $user->mobile=$request->get('mobile');
          $user->adress=$request->get('address');
          $user->age=$request->get('age');
        $user->userprice=$request->get('userprice');

          $user->gender=$request->get('gender');
          $user->status=$request->get('status')=='on'?'Active':'Blocked';
          if($request->hasFile('image')){
          $imagefile=$request->file('image');
          $imagename=time().' '.$request->name.' '.' '.$imagefile->getClientOriginalName();

          $imagefile->move('images/users',$imagename);
          $user->image=$imagename ;
        }

             $save= $user->save();
             if($save){
             $setting=Setting::findOrFail($user->id);
             $setting->create_order=$request->get('create_order')=='on'?'Visible':'InVisible';
             $setting->index_order=$request->get('index_order')=='on'?'Visible':'InVisible';
             $setting->product=$request->get('product')=='on'?'Visible':'InVisible';
             $setting->category=$request->get('category')=='on'?'Visible':'InVisible';
             $setting->vedio=$request->get('vedio')=='on'?'Visible':'InVisible';
             $setting->user_id=$user->id;
             $setSave= $setting->save();
             if($setSave){
             Alert::success('تم التعديل بنجاح', 'نجحت العملية');
             return redirect()->back();
             }

            }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return Alert::warning('لا يمكن حذف المستخدم','فشلت العملية');
    }

    public function password(Request $request,$id){

        $request->validate([
            'cuurent_password'=>'required|string|password:user',
            'newpassword'=>'required|string|min:8',
            'confairm_password'=>'required|string|same:newpassword',
        ],[

            'cuurent_password.required'=>'الرجاء كتابة كلمة السر الحالية',
            'cuurent_password.password'=>'كلمة السر الحالية خاطئة',
            'newpassword.required'=>'كلمة السر الجديدة فارغة',
            'newpassword.min'=>'كلمة السر الجديدة يجب أن تتجاوز ال8 أحرف',
            'confairm_password.required'=>'تأكيد كلمة السر الجديدة فارغة',
            'confairm_password.same'=>'تأكيد كلمة السرخاطئ'
        ]);

        $user=User::findOrFail($id);
        $user->view_password=$request->get('newpassword');
        $user->password=Hash::make($request->get('newpassword'));
        $save=$user->save();
        if($save){
        Alert::success('تم التعديل بنجاح', 'نجحت العملية');
        return redirect()->route('user.dashbord');
        }
    }
}
