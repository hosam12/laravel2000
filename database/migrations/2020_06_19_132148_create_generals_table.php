<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generals', function (Blueprint $table) {
            $table->id();
            $table->enum('create_order',['Visible','InVisible'])->default('Visible');
            $table->enum('index_order',['Visible','InVisible'])->default('Visible');
            $table->enum('product',['Visible','InVisible'])->default('Visible');
            $table->enum('category',['Visible','InVisible'])->default('Visible');
            $table->enum('vedio',['Visible',['InVisible']])->default('Visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generals');
    }
}
